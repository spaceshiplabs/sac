require("sails-test-helper");

describe(TEST_NAME, function() {
	
	describe("Relationships", function() {

		it("all contracts should have dependencias", function(done) {
			Contrato.count({dependencia2:null},function(e,results){
				if(e) throw(e);
				results.should.equal(0);
				done();
			});
		});

		it("all contracts should have empresa", function(done) {
			Contrato.count({provedorContratista:null},function(e,results){
				if(e) throw(e);
				results.should.equal(0);
				done();
			});
		});

		it("all contracts should have unidadCompradora", function(done) {
			Contrato.count({unidadCompradora:null},function(e,results){
				if(e) throw(e);
				results.should.equal(0);
				done();
			});
		});

		it("all contracts should have a moneda",function(done){
			Contrato.count({moneda:null},function(e,results){
				if(e) throw(e);
				results.should.equal(0);
				done();
			});
		});

        it("all contracts should have dependencias existentes",function(done){
            Dependencia.find().exec(function(err,results){
                if (err) throw(err);
                //console.log(results);
                var dependencias = results.map(function(element){
                    return element.id;
                });
                //console.log(dependencias);
                Contrato.count({ dependencia2 : dependencias}).exec(function(err,contratos){
                    if (err) throw(err);
                    Contrato.count().exec(function(err,allcontracts){
                        contratos.should.equal(allcontracts);
                        done();
                    });
                });
            });
        });

        it("all contracts should have unidades compradoras existentes",function(done){
            UnidadCompradora.find().exec(function(err,results){
                if (err) throw(err);
                //console.log(results);
                var unidades = results.map(function(element){
                    return element.id;
                });
                //console.log(dependencias);
                Contrato.count({ unidadCompradora : unidades}).exec(function(err,contratos){
                    if (err) throw(err);
                    Contrato.count().exec(function(err,allcontracts){
                        contratos.should.equal(allcontracts);
                        done();
                    });
                });
            });
        });

	});
});