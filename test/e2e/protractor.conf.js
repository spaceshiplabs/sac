var port =  process.env.PORT || 1337;
exports.config = {
  allScriptsTimeout: 11000,

  specs: ['**/*.js'],

  capabilities: {
    'browserName': 'chrome'
  },

  baseUrl: 'http://localhost:'+port+'/',

  framework: 'jasmine',

  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000
  }
};
