module.exports = {  
  run_hamming : function(){
    var hamming = require( 'compute-hamming' );
    var eliminated = [];
    // primero todas las empresas que sean identicas y lo unico diferente sea case
    Empresa.find().exec(function (err, companies) {
      var companies_array = companies;
      companies.forEach(function(company) {
        companies_array.forEach(function(company2) {
          if (company.id != company2.id && company.proveedor_contratista.length == company2.proveedor_contratista.length && eliminated.indexOf(company2.id) == -1) {
            var distance = hamming(company.proveedor_contratista.toUpperCase(), company2.proveedor_contratista.toUpperCase());
            var length = company.proveedor_contratista.length;
            if (distance == 0) {
              eliminated.push(company.id);
              Contrato.update({provedorContratista : company2.id}, {provedorContratista : company.id}).exec(function(err, updated) {
                Empresa.destroy({id:company2.id}, function(err, res) {
                  console.log("deleted: " + company2.id + " " + company2.proveedor_contratista);
                });
              });
            }
          }
        });
      });
    });

    //// primero todas las empresas que sean identicas y lo unico diferente sea case
    //Empresa.find({limit:1000}).exec(function (err, companies) {
    //  var companies_array = companies;
    //  companies.forEach(function(company) {
    //    companies_array.forEach(function(company2) {
    //      if (company.id != company2.id && company.proveedor_contratista.length == company2.proveedor_contratista.length) {
    //        var distance = hamming(company.proveedor_contratista.toUpperCase(), company2.proveedor_contratista.toUpperCase());
    //        var length = company.proveedor_contratista.length;
    //        if (distance < 4) {
    //          console.log(distance);
    //          console.log(company.proveedor_contratista);
    //          console.log(company2.proveedor_contratista);
    //        }
    //      }
    //    });
    //  });
    //});

    //console.log(DBService.naiveHammerDistance('acaw', 'acas'));
    //console.log("hehe");
    //console.log(hamming("TECNU", "BAKTU"));
  },
};