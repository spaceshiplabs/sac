var util = require('util');
var clipboard = require("copy-paste") 

var contrato = {};
var dependencias = [];
var readline = require('readline');
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

var file = 'deploy/dependenciaMappings.json';
var jf = require('jsonfile');
var mappings = {
  'contracts' : [],
  'dependencias' : [],
};

module.exports = {  
  importMappings : function(cb){
    mappings = readMappings();
    if(mappings){
      async.parallel([
        async.apply(updateContractsFromMap,mappings.contracts),
        async.apply(createDependenciasFromMap,mappings.dependencias)
      ],function(e,results){
        console.log(results[0].length+' contracts updated');
        console.log(results[1].length+' dependencias created');
        cb(e,results);
      });
    }
  },
  normalize : function(cb){    
    var stdin = process.openStdin();
    rl.on('line', readChoice);
    var mappings = readMappings();
    humanAsistedSearchFlow(cb);  
  }
};
var updateContractsFromMap = function(map,cb){
  async.mapSeries(map,function(mapping,cb){
    Contrato.update(mapping.query,mapping.updates,cb);
  },cb);
}
var createDependenciasFromMap = function(map,cb){
  async.mapSeries(map,Dependencia.findOrCreate,cb);
}
var readMappings = function(cb){
  try{
    return jf.readFileSync(file);
  }catch(e){
    return false;
  }
}

var readChoice = function (line) {

  var selection = line.toString();
  if(dependencias.length && contrato.dependencia){
    selection = parseInt(selection)-1;
    if(selection < dependencias.length){
      var id = dependencias[selection].id;
      updateContract({dependencia:contrato.dependencia},{dependencia2:id},humanAsistedSearchFlow);
    }if(selection == dependencias.length){
      dependencias = [];
      clipboard.copy(contrato.dependencia);
      console.log('Create: '+ contrato.dependencia+':'+guessSiglas(contrato)+'? (y or new name) ');
      util.print('>> ');
    }else{
      console.log('invalid value, try again');
      util.print('>> ');
    }
  }else if(contrato.dependencia){
    if(selection == 'y'){
      var newD = {
        dependencia : contrato.dependencia,
        extinct : true,
        siglas : guessSiglas(contrato),
      };

      Dependencia.create(newD).exec(function(e,dep){
        if(e) throw(e);
        mappings.dependencias.push(newD);
        jf.writeFileSync(file, mappings);
        console.log('new dependencia '+dep.dependencia+' created');
        humanAsistedSearchFlow();
      });      
    }else{
      var split = selection.split(':');
      var updates = {dependencia : split[0]};
      if(split[1]) updates.siglas = split[1];
      updateContract({dependencia:contrato.dependencia},updates,humanAsistedSearchFlow);
    }
  }
}

var updateContract = function(query,updates,cb){
  Contrato.update(query,updates,function(e,results){
    if(e) throw(e);
    console.log(results.length+' contracts modified');
    if(updates.updatedAt) delete updates.updatedAt;
    if(!updates.id){
      mappings.contracts.push({
        query : query,
        updates : updates
      });
      jf.writeFileSync(file, mappings);
    }
    cb(e,results);
  });
}
var getContratoWithoutDependencia = function(cb){
  Contrato.findOne({dependencia2:null}).exec(cb);
}

var guessDependenciasFromContract = function(con,cb){
  contrato = con;
  if(contrato){
    Dependencia.find({
      or : [
        {dependencia: { 'like': '%'+contrato.dependencia+'%' }},
        {siglas: { 'like': guessSiglas(contrato) }},
        {dependencia: { 'contains': contrato.dependencia }},
      ]
    }).exec(cb);          
  }else{
    cb(null,'done');
  }
}

var presentChoices = function(e,deps){
  dependencias = deps;
  if(e) throw(e);
  if(deps === 'done'){
    console.log('ur done dude');
  }else if(deps.length){
    if(deps.length > 1){
      console.log(deps.length+' dependencias found for: "'+contrato.dependencia+'"');
      deps.forEach(function(dep,key){
        var index = key+1;
        console.log(index+') '+dep.dependencia);
      });
      var last = deps.length+1;
      console.log(last+') insertar nombre');
      util.print('>> ');  
    }else if(deps.length == 1){
      console.log('dependencia found '+contrato.dependencia+' -> '+deps[0].dependencia);
      Contrato.update({dependencia:contrato.dependencia},{dependencia2:deps[0].id},function(e,res){
        if(e) throw(e);
        console.log(res.length+' contratos actualizados');
        humanAsistedSearchFlow();
      })
    }
  }else{
    clipboard.copy(contrato.dependencia);
    console.log(contrato.dependencia+' not found create with siglas: '+guessSiglas(contrato)+'? (y or new name) ');
    util.print('>> ');
  }
}

var humanAsistedSearchFlow = function(cb){
  async.waterfall([getContratoWithoutDependencia,guessDependenciasFromContract],presentChoices,cb);
}
var guessSiglas = function(contrato){
  return typeof(contrato.siglas) != 'undefined' ? contrato.siglas : contrato.codigo_contrato.toString().split('-')[0];
}