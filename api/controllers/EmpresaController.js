/**
 * EmpresaController
 *
 * @description :: Server-side logic for managing empresas
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  // Esta functión te regresa la cuenta total y el monto total de la busqueda compatible con el API que estas realizando
/*  find : function(req,res){
    var limit = (req.param('limit') === undefined) ? 50 : req.param('limit');
    var skip = (req.param('skip') === undefined) ? 0 : req.param('skip');
    var sort = (req.param('sort') === undefined) ? 'proveedor_contratista ASC' : req.param('sort');
    var prov = JSON.parse(req.param('where'));
    if(Object.getOwnPropertyNames(prov).length !== 0 && prov.proveedor_contratista === undefined){
      Dependencia.find().where({id: prov.dependencia2}).exec(function (err, dependencias) {
        var deps = [];
        dependencias.forEach(function(dep) { 
          deps.push({dependencia: dep.dependencia});
        });
        Contrato.native(function(err,collection){
          collection.aggregate([
              { "$match" : {$or: deps} },
              { "$sort": { "proveedor_contratista": -1 } },
              { "$group": { "_id": '$provedorContratista' } },
              { "$skip": parseInt(skip) },
              { "$limit": parseInt(limit) }
          ],function(err,docs) {
            var ids = [];
            docs.forEach(function(obj) { 
              ids.push(obj._id);
            });
            Empresa.find().where({_id: ids}).exec(function (err, response) {
              res.json(response);
            });
          });
        });
      });
    } else {
      var params = req.param('where');
      Empresa.find({ where: JSON.parse(params), limit: limit, skip: skip }).sort(sort).exec(function(err,empresas){
        res.json(empresas);
      });
    }
  },
  count : function(req,res){
    var params = req.params.all();
    //el id lo inserta req.paramas pero no es util siempre sera undefined
    delete params.id;
    var count_params = params;
    delete count_params.limit;
    delete count_params.skip;
    //ejecutar el count y el sum en paralelo con async
    var prov = JSON.parse(JSON.stringify(params.where));
    if(Object.getOwnPropertyNames(prov).length !== 0){
      Dependencia.find().where({id: prov.dependencia2}).exec(function (err, dependencias) {
        var deps = [];
        dependencias.forEach(function(dep) { 
          deps.push({dependencia: dep.dependencia});
        });
        Contrato.native(function(err,collection){
          collection.aggregate([
              { "$match" : {$or: deps} },
              { "$sort": { "proveedor_contratista": -1 } },
              { "$group": { "_id": '$provedorContratista' } }
          ],function(err,docs) {
            var length = (docs === undefined) ? 0 : docs.length;
            res.json({
              count: length,
            });
          });
        });
      });
    } else {
      var functions = [function(cb){Empresa.count(count_params).exec(cb)}];
      async.parallel(functions,function(e,result){
        if(e) throw(e);
        //formateamos un poco el resultado que nos regresa async y regresamos json
        res.json({
          count: result[0],
        });
      });
    }
  },*/
};

