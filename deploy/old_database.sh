#!/bin/bash

# variables
dataset_path=/home/juanku/juanku/personal/spaceshiplabs/datasets
repo_path=/home/juanku/juanku/personal/spaceshiplabs/repositorio/sac

# datasets
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.1.1.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.1.2.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.1.3.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.1.4.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.2.1.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.2.2.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.2.3.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.2.4.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.3.1.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.3.2.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.3.3.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.3.4.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.4.1.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.4.2.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.4.3.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.4.4.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.5.1.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2010-2013.5.2.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2014.1.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2014.2.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2014.3.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2014.4.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2014.5.csv" --type csv --headerline
#mongoimport -d pgp -c contrato2 --file "$dataset_path/compranet.2014.6.csv" --type csv --headerline

echo "processing 01"
mongo pgp "$repo_path/deploy/old_database.js"