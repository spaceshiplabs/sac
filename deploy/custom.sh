#!/bin/bash

# variables
dataset_path=/home/sonny/dev/sacdata/datasets
repo_path=/home/sonny/dev/sac

date1=$(date +"%s")

# cnetuc
# mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnetuc/Contratos2010_2012_150428084608.csv" --type csv --headerline
# mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnetuc/Contratos2013_150520075641.csv" --type csv --headerline
# mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnetuc/Contratos2014_150520075323.csv" --type csv --headerline
# mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnetuc/Contratos2015_150520075012.csv" --type csv --headerline

# set cnetuc source
echo "processing 00"
# mongo pgp "$repo_path/deploy/database/00.rename.js"

# set cnetuc source
echo "processing 01"
# mongo pgp "$repo_path/deploy/database/01.cnetuc.js"

# cnet3
# mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2002.csv" --type csv --headerline
 mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2003.csv" --type csv --headerline
# mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2004.csv" --type csv --headerline
# mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2005.csv" --type csv --headerline
# mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2006.csv" --type csv --headerline
# mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2007.csv" --type csv --headerline
# mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2008.csv" --type csv --headerline
# mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2009.csv" --type csv --headerline
# mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2010.csv" --type csv --headerline
# mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2011.csv" --type csv --headerline

# set cnet3 source
echo "processing 02"
 mongo pgp "$repo_path/deploy/database/02.cnet3.js"

# difference
# mongoimport -d pgp -c contrato --file "$dataset_path/cnet/difference.csv" --type csv --headerline

# set difference source
echo "processing 03.0"
#mongo pgp "$repo_path/deploy/database/03.0.difference.js"

# uc
mongoimport -d pgp -c uc --file "$dataset_path/UC_150421084549.csv" --type csv --headerline

# process data
echo "processing 04"
mongo pgp "$repo_path/deploy/database/04.index.js"
echo "processing 05"
mongo pgp "$repo_path/deploy/database/05.empresas.js"
echo "processing 06"
mongo pgp "$repo_path/deploy/database/06.00.dependencias.js"
echo "processing 06 01"
mongo pgp "$repo_path/deploy/database/06.01.index.js"
echo "processing 07"
mongo pgp "$repo_path/deploy/database/07.link_uc.js"
echo "processing 08"
mongo pgp "$repo_path/deploy/database/08.extract_duplicated.js"

date2=$(date +"%s")
diff=$(($date2-$date1))
echo "$(($diff / 60)) minutes and $(($diff % 60)) seconds elapsed."