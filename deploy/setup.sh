#!/bin/bash

# variables
dataset_path=/home/space/sac/datasets
repo_path=/home/space/www/sac/

user=""
password=""
if [[ "$1" == "-u" ]]; then
    user=$2
fi
if [ "$1" == "-p" ]; then
    password=$2
fi
if [ "$3" == "-u" ]; then
    user=$4
fi
if [ "$3" == "-p" ]; then
    password=$4
fi

if [[ (-z "$user") || (-z "$password") ]] ; then
    echo " error la funcion necesita credenciales de mongo "
    echo " ej. setup.sh -u USER -p 'PASS'"
    exit 1
fi

date1=$(date +"%s")

# cnetuc
mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnetuc/Contratos2010_2012_150428084608.csv" --type csv --headerline -u ${user} -p ${password} --authenticationDatabase pgp
mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnetuc/Contratos2013_150520075641.csv" --type csv --headerline -u ${user} -p ${password} --authenticationDatabase pgp
mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnetuc/Contratos2014_150520075323.csv" --type csv --headerline -u ${user} -p ${password} --authenticationDatabase pgp
mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnetuc/Contratos2015_150520075012.csv" --type csv --headerline -u ${user} -p ${password} --authenticationDatabase pgp

# set cnetuc source
echo "processing 00"
mongo pgp "$repo_path/deploy/database/00.rename.js" -u ${user} -p ${password} --authenticationDatabase pgp

# set cnetuc source
echo "processing 01"
mongo pgp "$repo_path/deploy/database/01.cnetuc.js" -u ${user} -p ${password} --authenticationDatabase pgp

# cnet3
mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2002.csv" --type csv --headerline -u ${user} -p ${password} --authenticationDatabase pgp
mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2003.csv" --type csv --headerline -u ${user} -p ${password} --authenticationDatabase pgp
mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2004.csv" --type csv --headerline -u ${user} -p ${password} --authenticationDatabase pgp
mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2005.csv" --type csv --headerline -u ${user} -p ${password} --authenticationDatabase pgp
mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2006.csv" --type csv --headerline -u ${user} -p ${password} --authenticationDatabase pgp
mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2007.csv" --type csv --headerline -u ${user} -p ${password} --authenticationDatabase pgp
mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2008.csv" --type csv --headerline -u ${user} -p ${password} --authenticationDatabase pgp
mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2009.csv" --type csv --headerline -u ${user} -p ${password} --authenticationDatabase pgp
mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2010.csv" --type csv --headerline -u ${user} -p ${password} --authenticationDatabase pgp
mongoimport -d pgp -c contrato --file "$dataset_path/cnet/cnet3/LP2011.csv" --type csv --headerline -u ${user} -p ${password} --authenticationDatabase pgp

# set cnet3 source
echo "processing 02"
mongo pgp "$repo_path/deploy/database/02.cnet3.js" -u ${user} -p ${password} --authenticationDatabase pgp

# difference
mongoimport -d pgp -c contrato --file "$dataset_path/cnet/difference.csv" --type csv --headerline -u ${user} -p ${password} --authenticationDatabase pgp

# set difference source
echo "processing 03.0"
mongo pgp "$repo_path/deploy/database/03.0.difference.js" -u ${user} -p ${password} --authenticationDatabase pgp

# uc load
mongoimport -d pgp -c uc --file "$dataset_path/UC_150421084549.csv" --type csv --headerline -u ${user} -p ${password} --authenticationDatabase pgp

# process data
echo "processing 04"
mongo pgp "$repo_path/deploy/database/04.index.js" -u ${user} -p ${password} --authenticationDatabase pgp
echo "processing 05"
mongo pgp "$repo_path/deploy/database/05.empresas.js" -u ${user} -p ${password} --authenticationDatabase pgp
echo "processing 06"
mongo pgp "$repo_path/deploy/database/06.00.dependencias.js" -u ${user} -p ${password} --authenticationDatabase pgp
echo "processing 06 01"
mongo pgp "$repo_path/deploy/database/06.01.link.js" -u ${user} -p ${password} --authenticationDatabase pgp
echo "processing 06 02"
mongo pgp "$repo_path/deploy/database/06.02.index.js" -u ${user} -p ${password} --authenticationDatabase pgp
#ya no se usa el link , pues lo hace en el archivo de dependencias
#echo "processing 07"
#mongo pgp "$repo_path/deploy/database/07.link_uc.js" -u ${user} -p ${password} --authenticationDatabase pgp
echo "processing 08"
mongo pgp "$repo_path/deploy/database/08.extract_duplicated.js" -u ${user} -p ${password} --authenticationDatabase pgp

date2=$(date +"%s")
diff=$(($date2-$date1))
echo "$(($diff / 60)) minutes and $(($diff % 60)) seconds elapsed."