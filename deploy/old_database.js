// indexes
db.contrato2.createIndex( { proveedor_contratista: 1 } );
db.contrato2.createIndex( { proveedor_contratista: 0 } );
db.contrato2.createIndex( { siglas: 1 } );
db.contrato2.createIndex( { siglas: 0 } );
db.contrato2.createIndex( { claveuc: 1 } );
db.contrato2.createIndex( { claveuc: 0 } );
db.contrato2.createIndex( { importe_contrato: 1 } );

// empresas
//counter=0;
//hashmap = {};
//db.contrato2.find({}).forEach(function (doc) {
//  if(hashmap[doc.proveedor_contratista] === undefined) {
//    hashmap[doc.proveedor_contratista] = counter;
//    date = new Date().toISOString();
//
//    id = new ObjectId();
//    obj = {proveedor_contratista: doc.proveedor_contratista, _id: id, createdAt: date, updatedAt: date};
//    db.empresa2.insert(obj);
//
//    db.contrato2.update({ _id: doc._id }, { $set: { provedorContratista: id } }, {multi: true, writeConcern: 0 });
//  }
//  counter++;
//  if(counter % 50000 === 0 ) {
//    print("ciclo " + counter);
//  }
//});
//print("insertando " + Object.keys(hashmap).length + " registros");


//db.contrato.createIndex( { codigo_contrato: 1 } );
//db.contrato.createIndex( { codigo_contrato: 0 } );
//
//counter=0;
//hashmap = {};
//db.contrato2.find({}).forEach(function (doc) {
//  db.contrato.find({codigo_contrato: doc.codigo_contrato}).forEach(function (nuevo) {
//    if(doc.proveedor_contratista != nuevo.proveedor_contratista){
//      id = new ObjectId();
//      obj = {codigo_contrato: doc.codigo_contrato, proveedor_contratista_viejo: doc.proveedor_contratista, proveedor_contratista_nuevo: nuevo.proveedor_contratista, _id: id};
//      db.contrato3.insert(obj); 
//    }
//
//    counter++;
//    if(counter % 50000 === 0 ) {
//      print("ciclo " + counter);
//    }
//  });
//});
