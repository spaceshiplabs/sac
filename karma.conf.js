var pipeline = require('./tasks/pipeline');
pipeline.jsAssetsAngular.push('assets/bower_components/angular-mocks/angular-mocks.js');
pipeline.jsAssets.push('test/frontend/**/*.js');
var js = pipeline.jsAssetsAngular.concat(pipeline.jsAssets);

module.exports = function(config){
  config.set({

    basePath : './',

    files : js,

    autoWatch : true,

    frameworks: ['mocha', 'chai'],

    browsers : ['Chrome'],

    plugins : [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-mocha',
            'karma-chai',
            'karma-junit-reporter'
            ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};
