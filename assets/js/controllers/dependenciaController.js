angular.module('contratobook').
  controller('dependenciaController',
  ['$scope','$mdSidenav','$http','$location',
  function($scope,$mdSidenav,$http,$location){
    $scope.init = function(){
      $scope.path = $location.path();
      $scope.getDependencias();
    };

    $scope.getDependencias = function() {
      $scope.params = {
        limit : 10000,
      };
      $scope.loading = true;
      $http({method:'GET',url:'/api/v1/dependencia',params:$scope.params}).then(function(response){
        $scope.loading = false;
        $scope.dependencias = response.data;
      });
      
    };
    
    $scope.init();
    
}]);