angular.module('contratobook').controller('companyController',['$scope','$mdSidenav','$http','$location', 'localStorageService', '$mdDialog', '$timeout',function($scope,$mdSidenav,$http,$location, localStorageService, $mdDialog, $timeout){
  var initializing = true
  $scope.init = function(){
    $scope.selectedItems = {
      'dependencia' : [],
    }
    var params = {};
    for (var key in $location.search()) {
      params = JSON.parse(key);
    }
    $scope.loading = false;
    $scope.currentMenuItem = 'home';
    $scope.path = $location.path();
    $scope.perPage = (params.limit === undefined) ? 50 : params.limit;
    $scope.where = {};
    $scope.pages = 0;
    $scope.loadingCount = true;
    $scope.skip = (params.skip === undefined) ? 0 : params.skip;
    $scope.currentPage = Math.round(($scope.skip / $scope.perPage) + 1);
    $scope.refresh();
  }
  $scope.getAutocomplete = function(val,field,objectName){
    var params = {where:{},limit:40};
    params.where[field] = {contains : val};
    return $http({method: 'GET', url: '/api/v1/'+objectName,params:params}).then(function(response){
      return response.data;
    });
  }
  $scope.query = function(val,field){
    if(val != "") {
      $scope.where[field] = {contains : val};
    } else {
      $scope.where = {};
    }
    $scope.params = {limit: $scope.perPage,skip:$scope.skip,sort:'importe_contrato DESC', where: $scope.where};
    $http({method: 'GET', url: '/api/v1/empresa',params:$scope.params}).then(function(response){
      $scope.loading = false;
      $scope.companies = response.data;
    });
    $location.search(JSON.stringify($scope.params));
    $scope.getCompanyMeta();
  }
  $scope.getCompanyMeta = function(){
    $scope.loadingCount = true;
    $http({method:'POST',url:'/api/v1/empresa/count',data:$scope.params}).then(function(response){
      $scope.pages = Math.ceil(response.data.count/$scope.perPage);
      if($scope.pages > 0) {
        $scope.loadingCount = false;
      }
    });
    $location.search(JSON.stringify($scope.params));
  }
  $scope.getCompanies = function() {
    $scope.where = { }
    $scope.skip = $scope.perPage * ($scope.currentPage - 1);
    $scope.params = {limit: $scope.perPage,skip:$scope.skip,sort:'importe_contrato DESC', where: $scope.where};
    $scope.loading = true;
    angular.extend($scope.params.where,$scope.formParams('dependencia','dependencia2'));
    
    $http({method:'GET',url:'/api/v1/empresa',params:$scope.params}).then(function(response){
      $scope.loading = false;
      $scope.companies = response.data;
    });
    $location.search(JSON.stringify($scope.params));
  }
  $scope.formParams = function(items,field){
    params = {};
    if($scope.selectedItems[items].length != 0){
      params[field] = [];
      $scope.selectedItems[items].forEach(function(item){
        params[field].push(item.id);
      });
    }
    return params;
  }
  $scope.refresh = function(){
    $scope.getCompanies();
    $scope.getCompanyMeta();
  }
  $scope.numberFormat = function(number){
    if(number === undefined || isNaN(number)) {
      number = "0";
    }
    number = Number(number);
    number = number.toFixed(2);
    return new Intl.NumberFormat().format(number);
  }
  
  $scope.toggleLeft = function() {
    $mdSidenav('left').toggle();
  }

  $scope.removeItem = function(key,item){
    for(x in $scope.selectedItems[key]){
      if($scope.selectedItems[key][x].id == item.id){
        $scope.selectedItems[key].splice(x,1);
        break;
      }
    }
    $scope.refresh();
  }
  $scope.prevPage = function(){
    $scope.currentPage -=1;
    $scope.currentPage = $scope.currentPage > 0 ? $scope.currentPage : $scope.pages;
    $scope.getCompanies();
    $scope.getCompanyMeta();
  }
  $scope.nextPage = function(){
    $scope.currentPage += 1;
    $scope.currentPage = $scope.currentPage <= $scope.pages ? $scope.currentPage : 1; 
    $scope.getCompanies();
    $scope.getCompanyMeta();
  }
  $scope.addItem = function(item,items){
    $scope.selectedItems = {
      'dependencia' : [],
    }
    $scope.currentPage = 1;
    $scope.selectedItems[items].push(item);
    $scope.refresh();   
  }
  $scope.removeItem = function(key,item){
    for(x in $scope.selectedItems[key]){
      if($scope.selectedItems[key][x].id == item.id){
        $scope.selectedItems[key].splice(x,1);
        break;
      }
    }
    $scope.refresh();
  }
  $scope.searchObjectsExists = function() {
    for (var item in $scope.selectedItems) {
      if($scope.selectedItems[item].length != 0) {
        return true;
      }
    }
    return false;
  };
  $scope.init();
}]);