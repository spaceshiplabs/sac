angular.module('contratobook').controller('contratoController',['$scope','$mdSidenav','$http','$location', '$mdDialog', '$timeout',function($scope,$mdSidenav,$http,$location, $mdDialog, $timeout){
  var initializing = true
  $scope.init = function(){
    $scope.selectedItems = {
      'proveedor_contratista' : [],
      'dependencia' : [],
      'nombre_de_la_uc' : [],
    }
    var params = {};
    for (var key in $location.search()) {
      params = JSON.parse(key);
    }
    $scope.loading = false;
    $scope.currentMenuItem = 'home';
    $scope.path = $location.path();
    $scope.perPage = (params.limit === undefined) ? 10 : params.limit;
    $scope.year = {start:2000,end:2016};
    $scope.sum = 0;
    $scope.sum_usd = 0;
    $scope.where = {};

    if(params.where !== undefined && params.where.fecha_inicio_year !== undefined && 
      params.where.fecha_inicio_year[">"] !== undefined && params.where.fecha_inicio_year["<"] !== undefined) {
      $scope.where['fecha_inicio_year'] = { '>': params.where.fecha_inicio_year[">"], '<': params.where.fecha_inicio_year["<"] };
      $scope.year.start = params.where.fecha_inicio_year[">"];
      $scope.year.end = params.where.fecha_inicio_year["<"];
    }
    // TODO: solo se debería hacer una petición a refresh
    if(params.where !== undefined && params.where.provedorContratista !== undefined) {
      $http({method:'GET',url:'/api/v1/empresa/'+params.where.provedorContratista}).then(function(response){
        $scope.selectedItems["proveedor_contratista"].push(response.data);
        $scope.refresh();
      });
    }
    if(params.where !== undefined && params.where.dependencia2 !== undefined) {
      $http({method:'GET',url:'/api/v1/dependencia/'+params.where.dependencia2}).then(function(response){
        $scope.selectedItems["dependencia"].push(response.data);
        $scope.refresh();
      });
    }
    if(params.where !== undefined && params.where.unidadCompradora !== undefined) {
      $http({method:'GET',url:'/api/v1/unidadcompradora/'+params.where.unidadCompradora}).then(function(response){
        $scope.selectedItems["nombre_de_la_uc"].push(response.data);
        $scope.refresh();
      });
    }

    $scope.loadingCount = true;
    $scope.pages = 0;
    $scope.skip = (params.skip === undefined) ? 0 : params.skip;
    $scope.currentPage = Math.round(($scope.skip / $scope.perPage) + 1);
    if($scope.path == '/widget'){
      $scope.contracts = window.widgetData;
    } else {
      $scope.refresh();
    }
    /*$scope.favorites = localStorageService.get('favorites');
    if($scope.favorites === undefined || $scope.favorites == null) {
      $scope.favorites = [];
      localStorageService.set('favorites', $scope.favorites);
    }*/
  }
  $scope.sliderup = function ($event) {
    $scope.currentPage = 1;
    $scope.refresh();
  };
  $scope.toggleContract = function(contract){
    if(contract.show != null) contract.show = !contract.show;
    else contract.show = true;
  }
  $scope.getAutocomplete = function(val,field,objectName){
    var params = {where:{},limit:40};
    params.where[field] = {contains : val};
    return $http({method: 'GET', url: '/api/v1/'+objectName,params:params}).then(function(response){
      return response.data;
    });
  }
  $scope.getContractMeta = function(){
    $http({method:'GET',url:'/api/v1/contrato/count',params:$scope.params}).then(function(response){
      $scope.pages = Math.ceil(response.data.count/$scope.perPage);
      $scope.loadingCount = ($scope.pages > 1) ? false : true;
    });
    $http({method:'GET',url:'/api/v1/contrato/sum',params:$scope.params}).then(function(response){
      $scope.sum = (response.data.sum > 999999) ? response.data.sum/1000000 : response.data.sum;
      $scope.sum_abbr = (response.data.sum > 999999) ? "MDP" : "";
    });
    
  }
  $scope.getContracts = function() {
    $scope.where = { "fecha_inicio_year": { ">": $scope.year.start, "<": $scope.year.end } };
    $scope.skip = $scope.perPage * ($scope.currentPage - 1);
    $scope.params = {limit: $scope.perPage,skip:$scope.skip,sort:'importe_contrato DESC', where: $scope.where};
    $scope.loading = true;
    angular.extend($scope.params.where,$scope.formParams('proveedor_contratista','provedorContratista'));
    angular.extend($scope.params.where,$scope.formParams('dependencia','dependencia2'));
    angular.extend($scope.params.where,$scope.formParams('nombre_de_la_uc','unidadCompradora'));
    
    $http({method:'GET',url:'/api/v1/contrato',params:$scope.params}).then(function(response){
      $scope.loading = false;
      $scope.contracts = response.data;
    });
    if ($scope.year.start != 2000 || $scope.year.end != 2016 || $scope.skip != 0 || Object.keys($scope.params.where).length > 1) {
      $location.search(JSON.stringify($scope.params));
    }
  }
  $scope.formParams = function(items,field){
    params = {};
    if($scope.selectedItems[items].length != 0){
      params[field] = [];
      $scope.selectedItems[items].forEach(function(item){
        params[field].push(item.id);
      });
    }
    return params;
  }
  $scope.addItem = function(item,items){
    console.log(item);
    $scope.selectedItems[items].push(item);
    $scope.currentPage = 1;
    $scope.refresh();
  }
  $scope.refresh = function(){
    $scope.getContracts();
    $scope.getContractMeta();
  }
  $scope.numberFormat = function(number){
    if(number === undefined || isNaN(number)) {
      number = "0";
    }
    number = Number(number);
    number = number.toFixed(2);
    return new Intl.NumberFormat().format(number);
  }
  
  $scope.toggleLeft = function() {
    $mdSidenav('left').toggle();
  }

  $scope.removeItem = function(key,item){
    for(x in $scope.selectedItems[key]){
      if($scope.selectedItems[key][x].id == item.id){
        $scope.selectedItems[key].splice(x,1);
        break;
      }
    }
    $scope.refresh();
  }
  $scope.prevPage = function(){
    $scope.currentPage -=1;
    $scope.currentPage = $scope.currentPage > 0 ? $scope.currentPage : $scope.pages;
    $scope.getContracts();
  }
  $scope.nextPage = function(){
    $scope.currentPage += 1;
    $scope.currentPage = $scope.currentPage <= $scope.pages ? $scope.currentPage : 1; 
    $scope.getContracts();
  }
/*  $scope.confirmRemoveBookmark = function(event, curr_bookmark) {
    $mdDialog.show(
      $mdDialog.alert()
        .title('Favoritos')
        .content('La busqueda actual ha sido eliminada de tus favoritos')
        .ariaLabel('Secondary click demo')
        .ok('Cerrar')
        .targetEvent(event)
    );
    
		for (var curr in $scope.favorites) {
		  if($scope.favorites[curr].name == curr_bookmark.name) {
		  	$scope.favorites.splice(curr, 1);
        localStorageService.set('favorites', $scope.favorites);
		  }
		}
  };
  $scope.bookmark = function(ev) {
    $mdDialog.show({
      targetEvent: ev,
      template:
        '<md-dialog aria-label="Favoritos">' +
          '<md-dialog-content class="sticky-container">' +
            '<h2 class="md-title ng-binding">Favoritos</h2>' +
            '<p class="ng-binding">Ingrese el nombre con el que desea guardar la busqueda</p>' +
            '<input id="bookmark_name" value="" type="text">' +
          '</md-dialog-content>' +
          '<div class="md-actions" layout="row">' +
            '<md-button ng-click="save()" class="md-primary">Cerrar</md-button>' +
          '</div>' +
        '</md-dialog>' ,
      controller: DialogController
    })
    .then(function(answer) {
    }, function() {
    });

    function DialogController($scope, $mdDialog, localStorageService) {
      $scope.save = function() {
        $scope.favorites = localStorageService.get('favorites');
        if($scope.favorites == null) {
          $scope.favorites = [];
        }
        $scope.favorites.push({ name: document.getElementById("bookmark_name").value, url: window.location.href });
        localStorageService.set('favorites', $scope.favorites);

        $timeout(function() {
            $scope.favorites = $scope.favorites;
            location.reload();
        }, 1000);
        $mdDialog.hide();
      };
      $scope.cancel = function() {
        $mdDialog.cancel();
      };
      $scope.answer = function(answer) {
        $mdDialog.hide(answer);
      };
    }
  };
  $scope.goToBookmark = function(url, event) {
    window.location.href = url;
  };*/
  $scope.searchObjectsExists = function() {
    for (var item in $scope.selectedItems) {
      if($scope.selectedItems[item].length != 0) {
        return true;
      }
    }
    return false;
  };
  $scope.init();
}]);