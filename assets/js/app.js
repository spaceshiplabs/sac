var app = angular.module('contratobook',['ngRoute','ngMaterial','ui.bootstrap','angular-packery','contentful']); 

app.config(['contentfulProvider',function(contentfulProvider){
	contentfulProvider.setOptions({
		space: 'wih8dujs4vq4',
		accessToken: '4f71400e47c59d849a2c5a435cd9eb38346e0e0c888f2c28385342876c6f00d9'
	});
}]);
app.config(['$routeProvider','$locationProvider',function($routeProvider, $locationProvider) {

  $locationProvider.html5Mode(true);

	$routeProvider.when('/',{
		templateUrl : 'templates/pages/home.html',
		controller : 'homeController',
		redirectTo: function(current, path, search){
      if(search.goto){
        // if we were passed in a search param, and it has a path
        // to redirect to, then redirect to that path
        return "/" + search.goto
      }
      else{
        // else just redirect back to this location
        // angular is smart enough to only do this once.
        return "/"
      }
    }
	}).when('/contratos', {
		templateUrl: 'templates/pages/contracts.html',
		controller : 'contratoController', 
		reloadOnSearch: false
	}).when('/empresas', {
		templateUrl: 'templates/pages/companies.html',
		controller : 'companyController', 
		reloadOnSearch: false
	}).when('/dependencias', {
		templateUrl: 'templates/pages/dependencias.html',
		controller : 'dependenciaController', 
		reloadOnSearch: false
	}).when('/widget', {
		templateUrl: 'templates/pages/widget.html',
		controller : 'contratoController',
	}).when('/contrato',{
		templateUrl: 'templates/pages/contract-single.html',
		controller : 'contratoController',
	}).otherwise({
		redirectTo: '/'
	});
}]);