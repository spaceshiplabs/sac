/*(function () {
    var controller = function($scope,$http){
       console.log('sidenav');

       $scope.selectedItems = {
        'proveedor_contratista' : [],
        'dependencia' : [],
        'nombre_de_la_uc' : [],
    }
    $scope.loading = false;
    $scope.currentMenuItem = 'home';
    $scope.path = $location.path();
    $scope.perPage = 25;
    $scope.year = {start:2002,end:2014};
    $scope.sum = 0;
    $scope.pages = 0;
    $scope.loadingCount = true;
    $scope.currentPage = 1;
    $scope.toggleContract = function(contract){
        if(contract.show != null) contract.show = !contract.show;
        else contract.show = true;
    }
    $scope.getAutocomplete = function(val,field,objectName){
        var params = {where:{},limit:7};
        params.where[field] = {contains : val};
        return $http({method: 'POST', url: '/api/v1/'+objectName+'/find',data:params}).then(function(response){
            return response.data;
        });
    }
    $scope.getContractMeta = function(){
        $scope.loadingCount = true;
        $http({method:'POST',url:'/api/v1/contrato/count',data:$scope.params}).then(function(response){
            $scope.pages = Math.ceil(response.data.count/$scope.perPage);
            $scope.sum = response.data.sum;
            $scope.loadingCount = false;
        });
    }
    $scope.getContracts = function(){
        $scope.skip = $scope.perPage * ($scope.currentPage - 1);
        $scope.params = {limit: $scope.perPage,skip:$scope.skip,sort:'importe_contrato DESC',where:{}};
        $scope.loading = true;      
        angular.extend($scope.params.where,$scope.formParams('proveedor_contratista','provedorContratista'));
        angular.extend($scope.params.where,$scope.formParams('dependencia','dependencia2'));
        angular.extend($scope.params.where,$scope.formParams('nombre_de_la_uc','unidadCompradora'));
        
        

        $http({method:'POST',url:'/api/v1/contrato/find',data:$scope.params}).then(function(response){
            $scope.loading = false;
            $scope.contracts = response.data;
        });
    }
    $scope.formParams = function(items,field){
        params = {};
        if($scope.selectedItems[items].length != 0){
            params[field] = [];
            $scope.selectedItems[items].forEach(function(item){
                params[field].push(item.id);
            });
        }
        return params;
    }
    $scope.addItem = function(item,items){
        $scope.selectedItems[items].push(item);
        $scope.refresh();       
    }
    $scope.refresh = function(){
        $scope.currentPage = 1;
        $scope.getContracts();
        $scope.getContractMeta();
    }
    $scope.numberFormat = function(number){
        return new Intl.NumberFormat().format(number.toFixed(2));
    }
    
    $scope.toggleLeft = function() {
        $mdSidenav('left').toggle();
    }

    $scope.removeItem = function(key,item){
        for(x in $scope.selectedItems[key]){
            if($scope.selectedItems[key][x].id == item.id){
                $scope.selectedItems[key].splice(x,1);
                break;
            }
        }
        $scope.refresh();
    }

       
    };

    controller.$inject = ['$scope','$http'];
    var directive = function () {
        return {
            controller : controller,
            scope : {
                params : '=',
            },
            templateUrl : '/templates/global/appSidebar.html',
        };
    };
    app.directive('appSidebar', directive);

}());
*/